const studentArray = [
    {name: "victor", surname: "victor", homeTask: 9},
    {name: "pro code", surname: "owner", homeTask: 9},
    {name: "vladislav", surname: "vladislav", homeTask: 5},
    {name: "im table", surname: "face-om ob table", homeTask: 8},
    {name: "artem", surname: "shewchuk", homeTask: 9},
    {name: "natalia", surname: "natalia", homeTask: 0},
    {name: "andrey", surname: "timoshenko", homeTask: 0},
    {name: "zyama", surname: "zyama", homeTask: 0},
    {name: "alex", surname: "green", homeTask: 0},
    {name: "inna", surname: "olshenko", homeTask: 0},
    {name: "alexandr", surname: "getmanski", homeTask: 0},
    {name: "vova", surname: "vova", homeTask: 9},
    {name: "yaroslav", surname: "yaroslav", homeTask: 0},
    {name: "mykilai", surname: "zaychebko", homeTask: 0},
    {name: "r1d", surname: "r1d", homeTask: 0},
    {name: "andrey", surname: "stehnii", homeTask: 0},
    {name: "denis", surname: "rudik", homeTask: 0}
];

let btnSwitcher = true;
let btnFilter = true;

const show = () => {
    btnSwitcher = !btnSwitcher;
    if ( !btnSwitcher ) {
        document.querySelector('button').innerText = "Скрыть список";
        document.querySelector('.filter').classList.remove('hidden');
        for (let i of studentArray) {
            let name = i.name;
            let surname = i.surname;
            let homeTask = i.homeTask;
        
            const html = `<li> 
                                <div class="grid-student-block">
                                    <p class="index-name">
                                        ${name}
                                    </p>
                                    <p class="index-surname">
                                        ${surname}
                                    </p>
                                    <p class="index-home-task">
                                        ${homeTask}
                                    </p>
                            </li>`;
            document.querySelector(".student-count>div>ol").insertAdjacentHTML('beforeend', html);
        }
    }
    else {
        document.querySelector('button').innerText = "Показать студентов";
        document.querySelector('.filter').classList.add('hidden');
        const sameArray = document.querySelectorAll('.student-count li');
        for (let i of sameArray)
        i.remove();
    }
}

const getResult = () => {
    btnFilter = !btnFilter;

    if ( !btnFilter ) {
        const sameArray = document.querySelectorAll('.student-count li');
        for (let i of sameArray) {
            i.remove();
        }
        let filterClass = document.querySelector('.filter');
        for (let i of studentArray) {
            let name = i.name;
            let surname = i.surname;
            let homeTask = i.homeTask;
            let homeTaskDone = Math.round(homeTask*80 / 100);
            console.log(homeTaskDone)
            if ( homeTaskDone >= Math.round((9/100)*80) ) {
                const html = `<li> 
                                    <div class="grid-student-block">
                                        <p class="index-name">
                                            ${name}
                                        </p>
                                        <p class="index-surname">
                                            ${surname}
                                        </p>
                                        <p class="index-home-task">
                                            ${homeTask}
                                        </p>
                                </li>`;
                document.querySelector(".student-count>div>ol").insertAdjacentHTML('beforeend', html);
            } 
        }
        filterClass.innerText = "Сбросить фильтр!";
        filterClass.classList.add('clean');
    }
    else {
        const sameArray = document.querySelectorAll('.student-count li');
        for (let i of sameArray) {
            i.remove();
        }
        document.querySelector('.filter').innerText = "Фильтровать > 80%";
        document.querySelector('.filter').classList.remove('clean');
        for (let i of studentArray) {
            let name = i.name;
            let surname = i.surname;
            let homeTask = i.homeTask;
        
            const html = `<li> 
                                <div class="grid-student-block">
                                    <p class="index-name">
                                        ${name}
                                    </p>
                                    <p class="index-surname">
                                        ${surname}
                                    </p>
                                    <p class="index-home-task">
                                        ${homeTask}
                                    </p>
                            </li>`;
            document.querySelector(".student-count>div>ol").insertAdjacentHTML('beforeend', html);
        }
    }
}